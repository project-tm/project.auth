<?php

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\EventManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Project\Core\Model\FavoritesTable,
    Project\Core\Model\RedirectTable;

IncludeModuleLangFile(__FILE__);

class project_auth extends CModule {

    public $MODULE_ID = 'project.auth';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_AUTH_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_AUTH_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_AUTH_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallEvent();
        Loader::includeModule($this->MODULE_ID);
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallEvent();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    /*
     * InstallEvent
     */

    public function InstallEvent() {
        $eventManager = EventManager::getInstance();
        $eventManager->registerEventHandler('main', 'OnAfterUserRegister', $this->MODULE_ID, '\Project\Auth\Event\User', 'OnAfterUserRegister');
        $eventManager->registerEventHandler('main', 'OnBeforeUserLogin', $this->MODULE_ID, '\Project\Auth\Event\User', 'OnBeforeUserLogin');
    }

    public function UnInstallEvent() {
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler('main', 'OnAfterUserRegister', $this->MODULE_ID, '\Project\Auth\Event\User', 'OnAfterUserRegister');
        $eventManager->unRegisterEventHandler('main', 'OnBeforeUserLogin', $this->MODULE_ID, '\Project\Auth\Event\User', 'OnBeforeUserLogin');
    }

}
