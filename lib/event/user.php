<?

namespace Project\Auth\Event;

use CUser,
    Project\Auth\Utility;

class User {

    public static function OnAfterUserRegister(&$arFields) {
        if (!empty($arFields['RESULT_MESSAGE'])) {
            $arFields['RESULT_MESSAGE']['MESSAGE'] = str_replace(str_replace('{login}', $arFields['LOGIN'], Config::DOUBLE), '', $arFields['RESULT_MESSAGE']['MESSAGE']);
        }
    }

    static public function OnBeforeUserLogin($arParams) {
        $res = CUser::GetByLogin($arParams['LOGIN']);
        if ($arUser = $res->GetNext()) {
            if (!empty($arUser['UF_PASSWORD'])) {
                if (Utility::hasgPassword($arUser['UF_PASSWORD'], $arParams)) {
                    $user = new CUser;
                    $user->Update($arUser["ID"], array(
                        "UF_PASSWORD" => '',
                        "PASSWORD" => $arParams['PASSWORD']
                    ));
                }
            }
        }
        return true;
    }

}
